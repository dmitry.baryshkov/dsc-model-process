#!/bin/sh

for bpp in 4 5 6 8
do
	for bpc in 8 10 12 14 16
	do
		echo "{ DSC_BPP(${bpp}), ${bpc},"
		cat /tmp/dsc/new/${bpp}bpp_${bpc}bpc_420.c
		echo "},"
	done
done
