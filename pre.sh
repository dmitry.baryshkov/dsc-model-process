#!/bin/sh

echo "{"

echo "{ 6, 8,"
cat /tmp/dsc/pre/6bpp_8bpc.c
echo "},"

for bpp in 8 10 12 15
do
	for bpc in 8 10 12
	do
		echo "{ ${bpp}, ${bpc},"
		cat /tmp/dsc/pre/${bpp}bpp_${bpc}bpc.c
		echo "},"
	done
done
echo "}"
