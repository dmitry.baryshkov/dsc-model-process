// SPDX-License-Identifier: MIT
/*
 * Copyright (c) 2023 Linaro Ltd.
 * Author: Dmitry Baryshkov <dmitry.baryshkov@linaro.org>
 */
use std::error::Error;
use std::fs::File;
use std::io::{BufRead, Write};

const DSC_NUM_BUF_RANGES: usize = 15;

#[derive(Default, Debug)]
struct RcModel {
    bpp: u8,
    bpc: u8,
    initial_xmit_delay: u16,
    first_line_bpg_offset: u8,
    initial_offset: u16,
    flatness_min_qp: u8,
    flatness_max_qp: u8,
    rc_quant_incr_limit0: u8,
    rc_quant_incr_limit1: u8,
    rc_min_qp: [i8; DSC_NUM_BUF_RANGES],
    rc_max_qp: [i8; DSC_NUM_BUF_RANGES],
    rc_bpg_offset: [i8; DSC_NUM_BUF_RANGES],
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = std::env::args();

    args.next();

    let infile = args.next().unwrap();
    let data = &std::fs::read(infile)?;

    let outfile = args.next();
    let mut out = match outfile {
        Some(f) => Box::new(File::create(&f)?) as Box<dyn Write>,
        None => Box::new(std::io::stdout()) as Box<dyn Write>,
    };

    let mut model = RcModel::default();

    for line in data.lines() {
        let line = line.unwrap();
        if line.is_empty() || line.starts_with("//") {
            continue;
        }
        let mut words = line.split_whitespace();
        let param = words.next().ok_or("no parameter name")?;
        let arg = words.next().ok_or("no argument found")?;
        match param {
            "RC_QUANT_INCR_LIMIT0" => model.rc_quant_incr_limit0 = arg.parse()?,
            "RC_QUANT_INCR_LIMIT1" => model.rc_quant_incr_limit1 = arg.parse()?,
            "INITIAL_FULLNESS_OFFSET" => model.initial_offset = arg.parse()?,
            "INITIAL_DELAY" => model.initial_xmit_delay = arg.parse()?,
            "FLATNESS_MIN_QP" => model.flatness_min_qp = arg.parse()?,
            "FLATNESS_MAX_QP" => model.flatness_max_qp = arg.parse()?,
            "FIRST_LINE_BPG_OFFSET" => model.first_line_bpg_offset = arg.parse()?,
            "BITS_PER_COMPONENT" => model.bpc = arg.parse()?,
            "BITS_PER_PIXEL" => {
                model.bpp = arg.parse()?;
                model.first_line_bpg_offset = if arg.parse::<i8>()? == 8 { 12 } else { 15 }
            }
            "RC_MINQP" => {
                model.rc_min_qp = arg
                    .split(',')
                    .map(|s| s.parse::<i8>().unwrap())
                    .collect::<Vec<i8>>()
                    .try_into()
                    .unwrap()
            }
            "RC_MAXQP" => {
                model.rc_max_qp = arg
                    .split(',')
                    .map(|s| s.parse::<i8>().unwrap())
                    .collect::<Vec<i8>>()
                    .try_into()
                    .unwrap()
            }
            "RC_OFFSET" => {
                model.rc_bpg_offset = arg
                    .split(',')
                    .map(|s| s.parse::<i8>().unwrap())
                    .collect::<Vec<i8>>()
                    .try_into()
                    .unwrap()
            }

            // ignore for now
            "RC_MODEL_SIZE"
            | "RC_BUF_THRESH"
            | "RC_TGT_OFFSET_HI"
            | "RC_TGT_OFFSET_LO"
            | "RC_EDGE_FACTOR"
            | "FLATNESS_DET_THRESH"
            | "SECOND_LINE_OFFSET_ADJ"
            | "SECOND_LINE_BPG_OFFSET" => {}
            _ => return Err(format!("unhandled parameter '{param}': {arg}").into()),
        }
    }

    //println!("{model:#?}");

    out.write_all(
        format!(
            "\t{{\n\t\t.bpp = DSC_BPP({}), .bpc = {},\n\t\t{{ {}, {}, {}, {}, {}, {}, {}, {{\n",
            model.bpp,
            model.bpc,
            model.initial_xmit_delay,
            model.first_line_bpg_offset,
            model.initial_offset,
            model.flatness_min_qp,
            model.flatness_max_qp,
            model.rc_quant_incr_limit0,
            model.rc_quant_incr_limit1
        )
        .as_bytes(),
    )?;

    let mut offset = 0;
    let mut buf = Vec::<String>::new();
    for i in 0..DSC_NUM_BUF_RANGES {
        let s = if i == DSC_NUM_BUF_RANGES - 1 {
            format!(
                "{{ {}, {}, {} }}",
                model.rc_min_qp[i], model.rc_max_qp[i], model.rc_bpg_offset[i]
            )
        } else {
            format!(
                "{{ {}, {}, {} }},",
                model.rc_min_qp[i], model.rc_max_qp[i], model.rc_bpg_offset[i]
            )
        };
        let len = s.len();
        if offset + buf.len() + len >= 80 - 16 {
            out.write_all(format!("\t\t\t{}\n", buf.join(" ")).as_bytes())?;
            buf = Vec::new();
            offset = 0;
        }
        buf.push(s);
        offset += len;
    }

    if !buf.is_empty() {
        out.write_all(format!("\t\t\t{}\n", buf.join(" ")).as_bytes())?;
    }
    out.write_all("\t\t\t}\n\t\t}\n\t},\n".as_bytes())?;

    out.flush()?;

    Ok(())
}
